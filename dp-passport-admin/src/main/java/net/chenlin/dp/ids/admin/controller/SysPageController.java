package net.chenlin.dp.ids.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 系统页面视图
 * @author zcl[yczclcn@163.com]
 */
@Controller
public class SysPageController {

	/**
	 * 首页
	 * @return
	 */
	@RequestMapping("/")
	public String index() {
		return "index.html";
	}

	/**
	 * 404页面
	 * @return
	 */
	@RequestMapping("/error/404")
	public String notFoundPage() {
		return "/error/404.html";
	}

	/**
	 * 403页面
	 * @return
	 */
	@RequestMapping("/error/403")
	public String noAuthPage() {
		return "/error/403.html";
	}

	/**
	 * 500页面
	 * @return
	 */
	@RequestMapping("/error/500")
	public String sysError() {
		return "/error/500.html";
	}

	/**
	 * 系统首页
	 * @return
	 */
	@RequestMapping("/dashboard")
	public String main() {
		return "/system/dashboard.html";
	}

}
