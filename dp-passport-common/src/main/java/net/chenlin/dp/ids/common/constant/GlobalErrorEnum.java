package net.chenlin.dp.ids.common.constant;

import net.chenlin.dp.ids.common.base.BaseResult;

/**
 * 全局错误信息
 * @author zcl<yczclcn@163.com>
 */
public enum GlobalErrorEnum {

    /**
     * 成功
     */
    SUCCESS("0000", "处理成功"),

    /**
     * 账户未登录
     */
    NOT_LOGIN("4001", "账户未登录"),

    /**
     * 账户已登录
     */
    HAS_LOGIN("4000", "账户已登录"),

    /**
     * 用户名不能为空
     */
    USERNAME_NOT_NULL("4002", "用户名不能为空"),

    /**
     * 密码不能为空
     */
    PASSWORD_NOT_NULL("4003", "密码不能为空"),

    /**
     * 当前用户不存在
     */
    USERNAME_ERROR("4004", "当前用户不存在"),

    /**
     * 密码错误
     */
    PASSWORD_ERROR("4005", "密码错误"),

    /**
     * 账户已被锁定
     */
    ACCOUNT_LOCKED("4006", "账户已被锁定"),

    /**
     * 您已被强制退出
     */
    ACCOUNT_EXIT("4007", "您已被强制退出"),

    /**
     * 系统异常
     */
    ERROR("9999", "系统异常"),

    /**
     * 业务异常
     */
    BizError("1000", "业务异常");

    /** 响应码 **/
    private String respCode;

    /** 提示信息 **/
    private String respMsg;

    /**
     * constructor
     * @param respCode
     * @param respMsg
     */
    GlobalErrorEnum(String respCode, String respMsg) {
        this.respCode = respCode;
        this.respMsg = respMsg;
    }

    /**
     * getter for respCode
     * @return
     */
    public String getRespCode() {
        return this.respCode;
    }

    /**
     * getter for respMsg
     * @return
     */
    public String getRespMsg() {
        return this.respMsg;
    }

    /**
     * getResult
     * @return
     */
    public BaseResult getResult() {
        return new BaseResult(this.respCode, this.respMsg);
    }

}
